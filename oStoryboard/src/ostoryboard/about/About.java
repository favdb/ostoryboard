/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.about;

import ostoryboard.i18n.I18Nmsg;
import ostoryboard.resources.images.Images;
import ostoryboard.oStoryboard;
import ostoryboard.ui.MainFrame;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class About extends JDialog {
	
	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	public void showDialog() {
		About dlg=new About();
		dlg.setVisible(true);
	}
	
	public About() {
		super();
		initialize();
	}
	
	public About(MainFrame m) {
		super(m);
		initialize();
	}
	
	private void initialize() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new MigLayout("fill"));
		setTitle(i18n.str("about")+" "+i18n.str("version")+" "+oStoryboard.VERSION);
		//info content
		InputStream in = About.class.getResourceAsStream("about.html");
		String texte="";
		try {
			texte = read(in);
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
		}
		JEditorPane edt=new JEditorPane();
		edt.setEditable(false);
		edt.setContentType("text/html");
		edt.setText(texte);
		edt.setPreferredSize(new Dimension(480,320));
		add(edt, "grow, wrap");
		//ok button
		JButton bt=new JButton(Images.getIcon("ok"));
		bt.setText(i18n.str("ok"));
		bt.addActionListener(e -> {
			dispose();
		});
		add(bt,"center");
		pack();
		this.setLocationRelativeTo(this.getParent());
		this.setModal(true);
	}
	
	public String read(InputStream input) throws IOException {
		InputStreamReader stream = new InputStreamReader(input);
		StringBuilder sb;
		try (BufferedReader reader = new BufferedReader(stream)) {
			sb = new StringBuilder();
			int ch;
			while ((ch = reader.read()) != -1) {
				sb.append((char) ch);
			}
		}
		return sb.toString();
	}

}

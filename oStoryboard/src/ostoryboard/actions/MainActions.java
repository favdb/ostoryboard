/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.actions;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import ostoryboard.ui.MainFrame;
import javax.swing.JComboBox;

/**
 *
 * @author favdb
 */
public class MainActions {

	private final MainFrame mainFrame;
	
	public MainActions(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
	}
	
	public void exec(ActionEvent e) {
		String name="";
		if (e.getSource() instanceof JMenuItem) {
			name=((JMenuItem)e.getSource()).getName();
		} else if (e.getSource() instanceof JButton) {
			name=((JButton)e.getSource()).getName();
		} else if (e.getSource() instanceof JComboBox) {
			name=((JComboBox)e.getSource()).getName();
		}
		switch(name) {
			case "file.new":
				mainFrame.createFile();
				break;
			case "file.open":
				mainFrame.openFile();
				break;
			case "file.save":
				mainFrame.saveFile();
				break;
			case "file.exit":
				System.exit(0);
				break;
			case "scene.new":
				mainFrame.addScene();
				break;
			case "scene.list":
				JComboBox cb=(JComboBox)e.getSource();
				String str=(String)cb.getSelectedItem();
				mainFrame.setScene(str);
				break;
			case "scene.first":
				mainFrame.setScene(0);
				break;
			case "scene.previous":
				mainFrame.setScene(-1);
				break;
			case "scene.next":
				mainFrame.setScene(1);
				break;
			case "scene.last":
				mainFrame.setScene(2);
				break;
				
		}
		
	}
	
}

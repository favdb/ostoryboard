/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ostoryboard;

import ostoryboard.about.About;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ostoryboard.ui.MainFrame;
import ostoryboard.util.Log;

/**
 *
 * @author favdb
 */
public class oStoryboard {
	
	public static String VERSION="0.1";
	private MainFrame mainFrame;
	public static Pref pref;
	
	public oStoryboard() {
		
	}
	
	public void init() {
		pref=new Pref();
		if (pref.getString(Pref.KEY.LAST_FILE).isEmpty()) {
			About dlg=new About();
			dlg.setVisible(true);
		}
		mainFrame=new MainFrame();
		mainFrame.setVisible(true);
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		if (args.length>0) {
			for (String arg:args) {
			if (arg.contains("--trace")) Log.setLevel(Log.DEVEL);
			if (arg.contains("--debug")) Log.setLevel(Log.DEBUG);
			}
		}
		try {
			for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					Font fnt = (Font) UIManager.getLookAndFeelDefaults().get("defaultFont");
					if (fnt != null) {
						float sz = fnt.getSize();
						Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
						int w = Toolkit.getDefaultToolkit().getScreenSize().width;
						float fc;
						if (d.width < 1080) {
							fc=(float) 1.0;
						} else if (d.width < 1245) {
							fc=(float) 1.2;
						} else if (d.width < 1935) {
							fc=(float) 1.4;
						} else {
							fc=(float) 1.6;
						}
						sz = (float) (sz * fc);
						Font fx = fnt.deriveFont(sz);
						UIManager.getLookAndFeelDefaults().put("defaultFont", fx);
					}
					break;
				}
			}
		} catch (ClassNotFoundException | IllegalAccessException
				| InstantiationException | UnsupportedLookAndFeelException e) {
		}

		SwingUtilities.invokeLater(() -> {
			oStoryboard demo = new oStoryboard();
			demo.init();
		});
	}

}

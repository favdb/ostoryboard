/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import ostoryboard.util.EnvUtil;

/**
 *
 * @author favdb
 */
public class Pref {
	
	List<PrefValue> preferences = new ArrayList<>();
	public enum KEY {
		LAST_FILE("LastFile",""),
		LAST_VIEW("LastView","");
		final private String key, value;

		private KEY(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getValue() {
			return (value);
		}

		public Integer getInteger() {
			return (Integer.parseInt(value));
		}

		public boolean getBoolean() {
			if (value.equals("1") || value.equals("true")) {
				return (true);
			} else {
				return (false);
			}
		}

		@Override
		public String toString() {
			return key;
		}
	}
	
	public Pref() {
		
	}
	
	public String get(KEY key) {
		return (getString(key));
	}

	public String getString(KEY key) {
		return (getString(key.toString()));
	}

	public String getString(String key) {
		for (PrefValue v : preferences) {
			if (v.key.equals(key)) {
				return (v.value);
			}
		}
		return ("");
	}
	
	public Integer getInterger(KEY key) {
		String v=get(key);
		if (v.isEmpty()) return 0;
		return Integer.parseInt(v);
	}
	
	public boolean getBoolean(KEY key) {
		String v=get(key);
		if (v.isEmpty()) return false;
		switch(v) {
			case "true": case "1": return true;
			default: return false;
		}
	}

	public char getChar(KEY key) {
		String v=get(key);
		if (v.isEmpty()) return ' ';
		return v.charAt(0);
	}
	
	public void load() {
		File file = (new File(System.getProperty("user.home")
			+ File.separator + ".storyboard"
			+ File.separator + "oStoryboard.ini"));
		if (!file.exists()) {
			create(file);
			return;
		}
		//App.trace("Load Preferences from " + file.getAbsolutePath());
		try {
			InputStream ips = new FileInputStream(file);
			InputStreamReader ipsr = new InputStreamReader(ips);
			try (BufferedReader br = new BufferedReader(ipsr)) {
				String ligne;
				while ((ligne = br.readLine()) != null) {
					String[] s = ligne.split("=");
					if (s.length < 2) {
						preferences.add(new PrefValue(s[0], ""));
					} else {
						if (isToLoad(s[0])) {
							preferences.add(new PrefValue(s[0], s[1]));
						}
					}
				}
				br.close();
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

	private void create(File file) {
		System.out.println("Create new Preferences in " + file.getAbsolutePath());
		try {
			EnvUtil.getPrefDir().mkdir();
			file.createNewFile();
		} catch (IOException e) {
			System.out.println("Unable to create new file " + file.getAbsolutePath());
		}
	}
	
	private boolean isToLoad(String key) {
		for (KEY k : KEY.values()) {
			if (k.toString().equals(key)) {
				return (true);
			}
		}
		return (false);
	}

	private static class PrefValue {

		String key;
		String value;

		public PrefValue(String k, String v) {
			key = k;
			value = v;
		}

		private PrefValue(KEY key) {
			this.key = key.toString();
			this.value = key.getValue();
		}

		public void set(String v) {
			value = v;
		}

		public String get() {
			return (value);
		}

		@Override
		public String toString() {
			return (key + "=" + value);
		}
	}

}

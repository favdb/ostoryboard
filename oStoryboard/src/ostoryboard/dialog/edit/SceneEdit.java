/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.dialog.edit;

import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import ostoryboard.model.Scene;
import ostoryboard.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class SceneEdit extends AbstractEdit {

	private JTextField tfName;
	private JTextField tfDuration;
	private Scene scene;
	
	public SceneEdit(MainFrame mainFrame) {
		super(mainFrame);
		initialize();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
	
	public void setScene(Scene scene) {
		this.scene=scene;
	}
	
	@Override
	public void initialize() {
		setLayout(new MigLayout("wrap 2"));
		setRow("name",tfName=initTextField("name",0),"growx");
		setRow("duration",tfDuration=initTextField("duration",3));
	}

	@Override
	public boolean check() {
		return(true);
	}

	@Override
	public void apply() {
	}

	@Override
	public void refresh() {
		tfName.setText(scene.getName());
		tfDuration.setText(scene.getDuration().toString());
	}
	
}

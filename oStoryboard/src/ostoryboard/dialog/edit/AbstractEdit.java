/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.dialog.edit;

import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ostoryboard.i18n.I18Nmsg;
import ostoryboard.ui.MainFrame;

/**
 *
 * @author favdb
 */
public abstract class AbstractEdit extends JDialog implements ActionListener {

	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	private final MainFrame mainFrame;
	
	public AbstractEdit(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
	}
	
	public abstract void initialize();
	
	public void setRow(JComponent component, String... grow) {
		add(component, "span");
	}
	
	public void setRow(String key, JComponent component, String... grow) {
		add(new JLabel(i18n.strColon(key)));
		if (grow.length>0) {
			add(component,grow[0]);
		}
		else {
			add(component);
		}
	}
	
	public JTextField initTextField(String name, int len) {
		JTextField tf=new JTextField();
		tf.setName(name);
		tf.setToolTipText(i18n.str(name));
		if (len>0) {
			tf.setColumns(len);
		}
		return(tf);
	}
	
	public abstract boolean check();
	
	public abstract void apply();
	
	public abstract void refresh();
	
}

/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.dialog.edit;

import java.awt.event.ActionEvent;
import ostoryboard.ui.MainFrame;

/**
 *
 * @author favdb
 */
public class PartEdit extends AbstractEdit {
	
	public PartEdit(MainFrame mainFrame) {
		super(mainFrame);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void initialize() {
	}

	@Override
	public boolean check() {
		return(true);
	}

	@Override
	public void apply() {
	}

	@Override
	public void refresh() {
	}
	
}

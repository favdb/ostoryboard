/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.dialog;

import ostoryboard.resources.images.Images;
import ostoryboard.ui.MainFrame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class NewFileDlg extends AbstractDialog {

	protected File file;
	private final boolean askForOverwrite;
	private JTextField tfName;
	private JTextField tfDir;
	private JButton btChooseDir;
	private JLabel lbWarning;

	public NewFileDlg(MainFrame m, boolean overwrite) {
		super(m);
		askForOverwrite = overwrite;
		initialize();
	}

	@Override
	public void initialize() {
		setLayout(new MigLayout("fill"));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle(i18n.str("project.new"));
        setModal(true);
		add(new JLabel(i18n.str("project.title")));
		tfName = new JTextField();
		tfName.setColumns(32);
		add(tfName,"span, growx");
		add(new JLabel(i18n.str("folder")));
		tfDir = new JTextField();
		tfDir.setColumns(32);
		tfDir.setEditable(false);
		add(tfDir);
		btChooseDir = new JButton();
		btChooseDir.setMargin(new Insets(0,0,0,0));
		btChooseDir.setIcon(Images.getIcon("file-open"));
		btChooseDir.setToolTipText(i18n.str("folder.choose"));
		btChooseDir.addActionListener((java.awt.event.ActionEvent evt) -> {
			btChooseDir();
		});
		add(btChooseDir,"wrap");
		lbWarning = new JLabel();
		lbWarning.setForeground(java.awt.Color.red);
		add(getCancelButton(), "span, split 2, right");
		add(getOkButton(), "right");
		this.pack();
		setLocationRelativeTo(mainFrame);
		setModal(true);
	}

	private void btChooseDir() {
		final JFileChooser fc = new JFileChooser(tfDir.getText());
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int ret = fc.showOpenDialog(this);
		if (ret != JFileChooser.APPROVE_OPTION) {
			return;
		}
		File dir = fc.getSelectedFile();
		tfDir.setText(dir.getAbsolutePath());
		lbWarning.setText("");
	}

	@Override
	public boolean apply() {
		File dir = new File(tfDir.getText());
		if (!dir.isDirectory() || !dir.canWrite() || !dir.canExecute()) {
			lbWarning.setText(i18n.str("file.not_writable"));
			return false;
		}
		String name = tfName.getText();
		if (!name.endsWith(".osbd")) {
			name += ".osbd";
		}
		file = new File(tfDir.getText() + File.separator + name);
		if ((file.exists()) && (askForOverwrite)) {
			int ret = JOptionPane.showConfirmDialog(this,
				i18n.str("file.save_overwrite_text") + "\n" + file.getName(),
				i18n.str("file.save_overwrite"),
				JOptionPane.YES_NO_OPTION);
			if (ret == JOptionPane.NO_OPTION) {
				lbWarning.setText(i18n.str("file.error.exists"));
				return false;
			}
		} else if (file.exists()) {
			lbWarning.setText(i18n.str("file.error.exists"));
				return false;
		}
		return true;
	}

	public String getProjectName() {
		return tfName.getText();
	}
	
	public File getFile() {
		return file;
	}
	
	public void setDefaultPath(String p) {
		tfDir.setText(p);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void refresh() {
	}
	
}

/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.dialog;

import javax.swing.JDialog;
import ostoryboard.i18n.I18Nmsg;
import ostoryboard.resources.images.Images;
import ostoryboard.ui.MainFrame;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author favdb
 */
public abstract class AbstractDialog extends JDialog implements ActionListener {
	
	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	public final MainFrame mainFrame;
	public boolean canceled=false;
	
	public AbstractDialog(MainFrame mainFrame) {
		super(mainFrame);
		this.mainFrame=mainFrame;
	}
	
	public abstract void initialize();

	public abstract void refresh();
	
	protected JButton getOkButton() {
		return getOkButton("ok");
	}
	
	protected JButton getOkButton(String txt) {
		JButton bt = new JButton(i18n.str(txt));
		bt.setIcon(Images.getIcon("ok"));
		bt.addActionListener(e -> {
			if (apply()) {
				canceled=false;
				dispose();
			}
		});
		return bt;
	}

	protected JButton getCancelButton() {
		JButton bt = new JButton(i18n.str("cancel"));
		bt.setIcon(Images.getIcon("cancel"));
		bt.addActionListener(e -> {
			canceled=true;
			dispose();
		});
		return bt;
	}
	
	public abstract boolean apply();

}

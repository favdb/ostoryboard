/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.model;

import java.util.ArrayList;
import java.util.List;
import ostoryboard.i18n.I18Nmsg;
import java.util.Arrays;

/**
 *
 * @author favdb
 */
public class Codes {
	public static final I18Nmsg i18n = I18Nmsg.getInstance();

	public enum STATUS {
		OUTLINE,
		DRAFT,
		EDIT1,
		EDIT2,
		DONE;
	}
	
	public static List getStatusList() {
		List<String> list=new ArrayList<>();
		for (STATUS st: STATUS.values()) {
			list.add(getStatus(st));
		}
		return list;
	}
	
	public static String getStatus(STATUS st) {
		return I18Nmsg.getInstance().str("status."+st.name().toLowerCase());
	}
	
	public enum SHOT {
		NONE,
		NORMAL,
		LOW_ANGLE,
		HIGH_ANGLE;
	}
	
	public static List<String> getShotList() {
		List<String> list=new ArrayList<>();
		for (SHOT shot:SHOT.values()) {
			list.add(shot.name());
		}
		return list;
	}
	
	public static String getShot(int shot) {
		return getShotList().get(shot);
	}
	
	public static int getShotByName(String name) {
		for (SHOT shot:SHOT.values()) {
			if (shot.name().equalsIgnoreCase(name)) {
				return shot.ordinal();
			}
		}
		return 0;
	}
	
	public static List getLightList() {
		List<String> list=new ArrayList<>();
		list.add(i18n.str("light.day"));
		list.add(i18n.str("light.night"));
		return list;
	}

	public static List getFocaleList() {
		String foc[] = {"12", "24", "35", "50", "70", "85", "100", "135"};
		List<String> list=new ArrayList<>();
		list.addAll(Arrays.asList(foc));
		return list;
	}

	public static List getStartList() {
		String foc[] = {"","CUT","FADE IN"};
		List<String> list=new ArrayList<>();
		list.addAll(Arrays.asList(foc));
		return list;
	}

	public static List getEndList() {
		String foc[] = {"","CUT","FADE OUT","DISSOLVE TO"};
		List<String> list=new ArrayList<>();
		list.addAll(Arrays.asList(foc));
		return list;
	}
	
	public static List getStageList() {
		List<String> list=new ArrayList<>();
		list.add("");
		for (int i=1;i<13;i++) {
			String s=i18n.str(String.format("stage.%02d", i));
			String sx[]=s.split(":");
			list.add(sx[0]);
			
		}
		return list;
	}

}

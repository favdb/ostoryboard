/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ostoryboard.i18n.I18Nmsg;
import ostoryboard.ui.table.Column;
import ostoryboard.util.DateUtil;
import java.util.Objects;

/**
 *
 * @author favdb
 */
public abstract class AbstractEntity {

	public static final I18Nmsg i18n = I18Nmsg.getInstance();

	public enum TYPE {
		NONE,
		PART,
		PERSON,
		SCENE,
		SEQUENCE;
	}
	
	public static String getTypeName(TYPE t) {
		return t.name().toLowerCase();
	}

	public enum DATUM {
		LONG,
		INTEGER,
		STRING,
		TEXT,
		DATE,
		ENTITY;
	}

	public Long id = -1L;
	public TYPE type = TYPE.NONE;
	public String name = "",
			created = "",
			maj = "",
			description = "",
			notes = "";

	public AbstractEntity() {
		this(-1L, TYPE.NONE, "");
	}

	public AbstractEntity(TYPE type) {
		this(-1L, type, "");
	}

	public AbstractEntity(Long id, TYPE type, String name) {
		this.id = id;
		this.type = type;
		this.name = name;
		this.created = DateUtil.getDate();
		this.maj = DateUtil.getDate();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TYPE getType() {
		return this.type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated() {
		this.created = DateUtil.getDate();
	}

	public void setCreated(Date date) {
		this.created = DateUtil.getDate(date);
	}

	public String getMaj() {
		return this.maj;
	}

	public void setMaj() {
		this.maj = DateUtil.getDate();
	}

	public void setMaj(Date date) {
		this.maj = DateUtil.getDate(date);
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (id != null ? id.hashCode() : 0);
		hash = hash * 31 + (name != null ? name.hashCode() : 0);
		hash = hash * 31 + (description != null ? description.hashCode() : 0);
		hash = hash * 31 + (notes != null ? notes.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AbstractEntity other = (AbstractEntity) obj;
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (!Objects.equals(this.description, other.description)) {
			return false;
		}
		if (!Objects.equals(this.notes, other.notes)) {
			return false;
		}
		return Objects.equals(this.id, other.id);
	}

	public List<Column> getColumns() {
		List<Column> cols = new ArrayList<>();
		int i = 0;
		cols.add(new Column(i++, "id", DATUM.LONG));
		cols.add(new Column(i++, "name", DATUM.STRING));
		cols.add(new Column(i++, "created", DATUM.STRING));
		cols.add(new Column(i++, "maj", DATUM.STRING));
		cols.add(new Column(i++, "description", DATUM.TEXT));
		cols.add(new Column(i++, "notes", DATUM.TEXT));
		return cols;
	}

	public List<Object> getRow() {
		List<Object> row = new ArrayList<>();
		row.add(id);
		row.add(type);
		row.add(name);
		row.add(created);
		row.add(maj);
		row.add(description);
		row.add(notes);
		return row;
	}
	
	public String toXml() {
		StringBuilder b=new StringBuilder("    <");
		b.append(getTypeName(type)).append("\n");
		b.append("   name=\"").append(getName()).append("\"\n");
		return b.toString();
	}
	
}

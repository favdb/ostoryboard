/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.model;

import java.util.ArrayList;
import java.util.List;
import ostoryboard.ui.table.Column;
import ostoryboard.util.ListUtil;
import ostoryboard.xml.XmlUtil;
import java.util.Objects;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 * @author favdb
 */
public class Scene extends AbstractEntity {

	private Integer number = -1;
	private String text = "";
	private Integer status = 0;
	private String img = "";
	private Integer duration = 0;
	private List<Person> persons = new ArrayList<>();
	public String pitch = "";
	public Integer moment = 0, //0=NONE, 1=DAY, 2=AT DAWN, 3=MORNING, 4=MIDDAY,
			//5=AFTERNOON, 6=SUNSET, 7=NIGHT, 8=EVENING, 9=LATER
			focal = 0, //the focal in 12,24,35,50,70,85,100,135
			loc = 0, //0=NONE,1=INT.,2=EXT.
			start = 0, //0=NONE,1=CUT,2=FADE IN
			end = 0, //0=NONE,1=CUT,2=FADE OUT,3=DISSOLVE TO
			stage = 0, //0=none, else the index number of the stage
			shot = 0,
			seq = 1,
			plan = 1;
	private String mvt = "";	//any combination of F=Fixed, P=Panorama, T=Travelling
	// S=Steadicam, G=Grue, Z=Zoom, D=Drone

	public Scene() {
		super(TYPE.SCENE);
		setName(i18n.str("scene") + " ?");
	}

	public Scene(Node node) {
		super(TYPE.SCENE);
		NamedNodeMap attribs = node.getAttributes();
		setNumber(XmlUtil.attributeGetInteger(node, "number"));
		setSeq(XmlUtil.attributeGetInteger(node, "seq"));
		setPlan(XmlUtil.attributeGetInteger(node, "plan"));
		setDuration(XmlUtil.attributeGetInteger(node, "dur"));
		setShot(XmlUtil.attributeGetInteger(node, "shot"));
		setImg(XmlUtil.attributeGetString(node, "img"));
		setFocal(XmlUtil.attributeGetInteger(node, "foc"));
		setMvt(XmlUtil.attributeGetString(node, "mvt"));
		setMoment(XmlUtil.attributeGetInteger(node, "moment"));
		setLoc(XmlUtil.attributeGetInteger(node, "loc"));
		setNotes(XmlUtil.childContentGet(node, "notes"));
	}

	public Scene(int seq, int plan) {
		this();
		setSeq(seq);
		setPlan(plan);
	}
	
	@Override
	public String getName() {
		return String.format("%02d/%02d", getSeq(), getPlan());
	}

	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public boolean hasPersons() {
		return !persons.isEmpty();
	}

	public void addPerson(Person person) {
		if (persons.contains(person)) {
			return;
		}
		persons.add(person);
	}

	public void removePerson(Person person) {
		if (persons.contains(person)) {
			persons.remove(person);
		}
	}

	public String personsToString() {
		List<String> list = new ArrayList<>();
		for (Person p : persons) {
			list.add(p.name);
		}
		return ListUtil.join(list);
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getPitch() {
		return this.pitch;
	}

	public void setPitch(String pitch) {
		this.pitch = pitch;
	}

	public Integer getMoment() {
		return this.moment;
	}

	public void setMoment(Integer moment) {
		this.moment = moment;
	}

	public Integer getLoc() {
		return this.loc;
	}

	public void setLoc(Integer loc) {
		this.loc = loc;
	}

	public Integer getStage() {
		return this.stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public Integer getStart() {
		return this.start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public Integer getEnd() {
		return this.end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public Integer getFocal() {
		return this.focal;
	}

	public void setFocal(int focal) {
		this.focal = focal;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getMvt() {
		return mvt;
	}

	public void setMvt(String mvt) {
		this.mvt = mvt;
	}

	@Override
	public List<Column> getColumns() {
		List<Column> cols = super.getColumns();
		int i = cols.size();
		cols.add(new Column(i++, "duration", DATUM.INTEGER));
		cols.add(new Column(i++, "img", DATUM.STRING));
		return cols;
	}

	@Override
	public List<Object> getRow() {
		List<Object> row = super.getRow();
		row.add(duration);
		row.add(img);
		return row;
	}

	public String getScenarioHeader() {
		StringBuilder b = new StringBuilder();
		b.append("<p>");
		if (loc > 0) {
			b.append(" ");
			b.append(i18n.str(String.format("%s.%02d", "scenario.loc", loc)));
		} else {
			b.append(" ");
			b.append(i18n.str(String.format("%s.%02d", "scenario.loc", 2)));
		}
		if (moment > 0) {
			b.append(" ");
			b.append(i18n.str(String.format("%s.%02d", "scenario.moment", moment))).append(" - ");
		}
		if (loc != null) {
			b.append(" ");
			b.append(loc.toString());
		}
		b.append(" - ");
		if (start > 0) {
			b.append(i18n.str(String.format("%s.%02d", "scenario.in", start)));
		}
		b.append("</p>");
		return (b.toString());
	}

	public String getScenarioFooter() {
		StringBuilder b = new StringBuilder();
		b.append("<p style=\"text-align:right;\">");
		if (end > 0) {
			b.append(i18n.str(String.format("%s.%02d", "scenario.out", end)));
		}
		b.append("</p>");
		return (b.toString());
	}

	public void setShot(int shot) {
		this.shot = shot;
	}

	public Integer getShot() {
		return this.shot;
	}

	public String getShotString() {
		return Codes.getShot(this.shot);
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public Integer getSeq() {
		return this.seq;
	}

	public void setPlan(int plan) {
		this.plan = plan;
	}

	public Integer getPlan() {
		return this.plan;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (seq != null ? seq.hashCode() : 0);
		hash = hash * 31 + (plan != null ? plan.hashCode() : 0);
		hash = hash * 31 + (duration != null ? duration.hashCode() : 0);
		hash = hash * 31 + (shot != null ? shot.hashCode() : 0);
		hash = hash * 31 + (start != null ? start.hashCode() : 0);
		hash = hash * 31 + (end != null ? end.hashCode() : 0);
		hash = hash * 31 + (focal != null ? focal.hashCode() : 0);
		hash = hash * 31 + (img != null ? img.hashCode() : 0);
		hash = hash * 31 + (loc != null ? loc.hashCode() : 0);
		hash = hash * 31 + (moment != null ? moment.hashCode() : 0);
		hash = hash * 31 + (persons != null ? persons.hashCode() : 0);
		hash = hash * 31 + (pitch != null ? pitch.hashCode() : 0);
		hash = hash * 31 + (stage != null ? stage.hashCode() : 0);
		hash = hash * 31 + (status != null ? status.hashCode() : 0);
		hash = hash * 31 + (text != null ? text.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Scene other = (Scene) obj;
		if (!Objects.equals(this.shot, other.shot)) {
			return false;
		}
		if (!Objects.equals(this.seq, other.seq)) {
			return false;
		}
		if (!Objects.equals(this.plan, other.plan)) {
			return false;
		}
		if (!Objects.equals(this.text, other.text)) {
			return false;
		}
		if (!Objects.equals(this.img, other.img)) {
			return false;
		}
		if (!Objects.equals(this.pitch, other.pitch)) {
			return false;
		}
		if (!Objects.equals(this.number, other.number)) {
			return false;
		}
		if (!Objects.equals(this.status, other.status)) {
			return false;
		}
		if (!Objects.equals(this.duration, other.duration)) {
			return false;
		}
		if (!Objects.equals(this.persons, other.persons)) {
			return false;
		}
		if (!Objects.equals(this.moment, other.moment)) {
			return false;
		}
		if (!Objects.equals(this.focal, other.focal)) {
			return false;
		}
		if (!Objects.equals(this.loc, other.loc)) {
			return false;
		}
		if (!Objects.equals(this.start, other.start)) {
			return false;
		}
		if (!Objects.equals(this.end, other.end)) {
			return false;
		}
		return Objects.equals(this.stage, other.stage);
	}

	@Override
	public String toXml() {
		this.setName(String.format("%02d/%02d", getSeq(), getPlan()));
		StringBuilder b = new StringBuilder(super.toXml());
		b.append(XmlUtil.toXmlAttribute("seq", getSeq()));
		b.append(XmlUtil.toXmlAttribute("plan", getPlan()));
		b.append(XmlUtil.toXmlAttribute("dur", getDuration()));
		b.append(XmlUtil.toXmlAttribute("shot", getShot()));
		b.append(XmlUtil.toXmlAttribute("img", getImg()));
		b.append(XmlUtil.toXmlAttribute("foc", getFocal()));
		b.append(XmlUtil.toXmlAttribute("mvt", getMvt()));
		b.append(XmlUtil.toXmlAttribute("moment", getMoment()));
		b.append(XmlUtil.toXmlAttribute("loc", getLoc()));
		b.append("       >\n");
		b.append(XmlUtil.stringToChild(2, "notes", notes));
		b.append("    </scene>\n");
		return b.toString();
	}

}

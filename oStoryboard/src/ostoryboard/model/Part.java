/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.model;

import java.util.ArrayList;
import java.util.List;
import ostoryboard.ui.table.Column;

/**
 *
 * @author favdb
 */
public class Part extends AbstractEntity {
	
	private Part sup=null;
	
	public Part() {
		super(TYPE.PART);
	}
	
	public Part(String name) {
		super(-1L,TYPE.PART,name);
	}
	
	public boolean hasSup() {
		return sup!=null;
	}
	
	public Part getSup() {
		return this.sup;
	}
	
	public void setSup(Part part) {
		this.sup=part;
	}

	@Override
	public List<Column> getColumns() {
		List<Column> cols=new ArrayList<>();
		int i=cols.size();
		cols.add(new Column(i++,"sup",DATUM.ENTITY));
		return cols;
	}

	@Override
	public List<Object> getRow() {
		List<Object> row=super.getRow();
		row.add(sup);
		return row;
	}
	
}

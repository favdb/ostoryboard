/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.resources.images;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author favdb
 */
public class Images {

	private static final String DIR = "icons/";

	/**
	 * Gets the icon at the specified path
	 *
	 * @param path The path of the icon
	 * @return The icon, or null if the icon file doesn't exist
	 */
	public static ImageIcon getIcon(String path) {
		ImageIcon icon = createImageIcon(DIR + path.replaceAll("\\.", "_") + ".png");
		// resize icon for screen definition
		if (icon!=null) icon = resizeIcon(icon);
		return (icon);
	}

	/**
	 * Gets the icon at the specified path for the specified size
	 *
	 * @param path The path of the icon
	 * @param size: fixed size
	 * @return The icon, or null if the icon file doesn't exist
	 */
	public static ImageIcon getIcon(String path, int size) {
		ImageIcon icon = createImageIcon(DIR + path.replaceAll("\\.", "_") + ".png");
		// resize icon for screen definition
		if (icon!=null) icon = resizeIcon(icon, size);
		return (icon);
	}

	/**
	 * Create an ImageIcon from the image at the specified path
	 *
	 * @param path The path of the image
	 * @return The image icon, or null if the image doesn't exist
	 */
	public static ImageIcon createImageIcon(String path) {

		URL u = Images.class.getResource(path);
		if (u == null) {
			u = Images.class.getResource(DIR + "unknown.png");
		}
		ImageIcon img = null;
		try {
			img = new ImageIcon(u);
		} catch (Exception ex) {
			System.err.println("unable to find " + path);
		}
		return img;
	}

	public static ImageIcon createExternalImageIcon(String path) {
		ImageIcon img = null;
		try {
			img = new ImageIcon(path);
		} catch (Exception ex) {
			//System.err.println("unable to find " + path);
		}
		return img;
	}

	/**
	 * resize the ImageIcon to fit to screen dimension
	 *
	 * @param icon
	 * @return
	 */
	public static ImageIcon resizeIcon(ImageIcon icon) {
		int sz;
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int w=Toolkit.getDefaultToolkit().getScreenSize().width;
		if (d.width < 1245) {
			sz = 16;
		} else if (d.width < 1935) {
			sz = 24;
		} else {
			sz = 32;
		}

		ImageIcon rs = icon;
		if ((icon.getIconHeight() != sz) || (icon.getIconWidth() != sz)) {
			double imageHeight = icon.getIconHeight();
			double imageWidth = icon.getIconWidth();
			int sw = sz, sh = sz;

			if (imageHeight / sz > imageWidth / sz) {
				sw = (int) (sh * imageWidth / imageHeight);
			} else {
				sh = (int) (sw * imageHeight / imageWidth);
			}
			Image imageicon = icon.getImage().getScaledInstance(sw, sh, Image.SCALE_DEFAULT);
			rs = new ImageIcon(imageicon);
		}
		return (rs);
	}

	/**
	 * resize the ImageIcon to fit to screen dimension
	 *
	 * @param icon
	 * @param size
	 * @return
	 */
	public static ImageIcon resizeIcon(ImageIcon icon, int size) {
		ImageIcon rs = icon;
		if ((icon.getIconHeight() != size) || (icon.getIconWidth() != size)) {
			double imageHeight = icon.getIconHeight();
			double imageWidth = icon.getIconWidth();
			int sw = size, sh = size;

			if (imageHeight / size > imageWidth / size) {
				sw = (int) (sh * imageWidth / imageHeight);
			} else {
				sh = (int) (sw * imageHeight / imageWidth);
			}
			Image imageicon = icon.getImage().getScaledInstance(sw, sh, Image.SCALE_DEFAULT);
			rs = new ImageIcon(imageicon);
		}
		return (rs);
	}

	/**
	 * resize the ImageIcon to fit to screen dimension
	 *
	 * @param icon
	 * @param size
	 * @return
	 */
	public static ImageIcon resizeIcon(ImageIcon icon, Dimension size) {
		Image imageicon = icon.getImage().getScaledInstance(size.width, size.height, Image.SCALE_DEFAULT);
		return (new ImageIcon(imageicon));
	}

}

/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.xml;

import ostoryboard.i18n.I18Nmsg;
import ostoryboard.model.Scene;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author favdb
 */
public class XmlFile {

	public static final I18Nmsg i18n = I18Nmsg.getInstance();

	private String fileName="";
	private File file;
	private List<Scene> scenes=null;
	private boolean modified;
	private boolean opened;
	private DocumentBuilder documentBuilder;
	private Document document;
	private Element rootNode;
	private String rootName;

	public XmlFile(String fileName) {
		this.fileName = fileName;
		scenes = null;
	}

	public XmlFile() {
	}

	public boolean create(String fileName) {
		File f = new File(fileName);
		if (f.exists()) {
			JOptionPane.showConfirmDialog(null,
					i18n.str("file.exists"),
					i18n.str("file.create"),
					JOptionPane.YES_OPTION,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		this.file = f;
		//create an empty file
		this.fileName = fileName;
		this.opened = true;
		return true;
	}

	public boolean open(String fileName) {
		File f = new File(fileName);
		if (!f.exists()) {
			JOptionPane.showConfirmDialog(null,
					i18n.str("file.not_exists"),
					i18n.str("file.open"),
					JOptionPane.YES_OPTION,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		opened = false;
		documentBuilder = null;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			System.err.println("DocumentBuilder error:\n" + ex.getLocalizedMessage());
			return (false);
		}
		document = readDom();
		if (document == null) {
			rootNode = null;
			return (false);
		}
		rootNode = document.getDocumentElement();
		Element n = (Element) rootNode.getElementsByTagName("storyboard").item(0);
		if (n != null) {
			rootName = n.getFirstChild().getNodeName();
		}
		opened = true;
		this.file = f;
		this.fileName = fileName;
		this.opened = true;
		return true;
	}

	public Document readDom() {
		//App.trace("Importer.readDom()");
		Document rc = null;
		try {
			rc = documentBuilder.parse(new File(fileName));
		} catch (SAXException e) {
			System.err.println("Parsing error for " + fileName + "\n" + e.getMessage());
		} catch (IOException e) {
			System.err.println("I/O error for " + fileName + "\n" + e.getMessage());
		}
		return (rc);
	}

	public void setModified() {
		this.modified = true;
	}

	public boolean isModified() {
		return this.modified;
	}

	public void close() {
		if (!opened) {
			return;
		}
		if (modified) {
			Object[] choix = {
				i18n.str("cancel"),
				i18n.str("file.save"),
				i18n.str("ignore"),};
			int n = JOptionPane.showOptionDialog(null,
					i18n.str("close.confirm"),
					i18n.str("close"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null, choix, choix[0]);
			if (n == 0) {
				return;
			}
			if (n == 1) {
				saveFile();
			}
		}
	}

	public boolean initFile() {
		scenes = new ArrayList<>();
		//create file
		//write root header
		//close file
		return true;
	}

	public boolean loadFile() {
		scenes = new ArrayList<>();
		//read scenes
		NodeList children = rootNode.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) children.item(i);
				String tag = element.getTagName();
				if (tag.equals("scene")) {
					scenes.add(new Scene((Node)element));
				}
			}
		}
		//close file
		return true;
	}

	public boolean saveFile() {
		//open file for writing
		//write root header
		//write the scenes
		//close file
		return true;
	}
	
}

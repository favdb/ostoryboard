/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.xml;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author favdb
 */
public class XmlUtil {
	
	public static String createFileHeader() {
		String str="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n";
		str+="<storyboard>\n";
		return str;
	}
	
	public static String toXmlAttribute(String key, String value) {
		StringBuilder b=new StringBuilder();
		b.append("       ").append(key).append("=\"").append(value).append("\"\n");
		return b.toString();
	}

	public static String toXmlAttribute(String key, Integer value) {
		return toXmlAttribute(key, value.toString());
	}

	/** retourne le String d'un attribut
	 * 
	 * @param node : noeud
	 * @param attribut : attribut
	 * @param def : valeur par défaut en cas d'absence du noeud, facultatif
	 * @return 
	 */
	public static String attributeGetString(Node node, String attribut, String... def) {
		String val="";
		if (def!=null && def.length>0) val=def[0];
		if (node != null) {
			val = ((Element)node).getAttribute(attribut);
		}
		return(val.trim());
	}
	
	/** récupération d'une valeur Integer d'un attribut ou la valeur par défaut en cas d'absence
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param def : valeur par défaut en cas d'absence du noeud, facultatif
	 * @return la valeur Integer ou la valeur par défaut en cas d'absence
	 */
	public static Integer attributeGetInteger(Node node, String attribute, int... def) {
		String x = attributeGetString(node, attribute);
		if (x.isEmpty() || "null".equals(x)) {
			if (def!=null && def.length>0) {
				return(def[0]);
			}
			return (0);
		}
		return (Integer.parseInt(x));
	}

	/** retourne une valeur Long d'un attribut avec une valeur par défaut en cas d'absence
	 * 
	 * @param node : noeud
	 * @param attribute : nom de l'attribut
	 * @param def : valeur par défaut en cas d'absence, facultatif
	 * @return la valeur Long de l'attributou la valeur par défaut
	 */
	public static Long attributeGetLong(Node node, String attribute, long... def) {
		String x = attributeGetString(node, attribute);
		if (x.isEmpty() || "null".equals(x) || !isLong(x)) {
			if (def!=null && def.length>0) {
				return (def[0]);
			} return(-1L);
		}
		return (Long.parseLong(x));
	}
	
	public static boolean isLong(String x) {
		try {
			Long d = Long.parseLong(x);
		} catch (NumberFormatException ex) {
			return false;
		}
		return true;
	}

	/**
	 * récupération des données d'un noeud enfant
	 *
	 * @param node	: noeud parent
	 * @param n : nom du noeud enfant
	 * @return : une chaine de caractère
	*
	 */
	public static String childContentGet(Node node, String n) {
		NodeList t = node.getChildNodes();
		if (t.getLength() > 0) {
			for (int i = 0; i < t.getLength(); i++) {
				if (t.item(i).getNodeName().equals(n)) {
					return (t.item(i).getTextContent().trim());
				}
			}
		}
		return ("");
	}

	public static String stringToChild(int tab, String key, String value) {
		String stab="";
		for (int i=0;i<tab;i++) stab+="    ";
		if (value == null || value.trim().isEmpty()) {
			return ("");
		}
		StringBuilder b = new StringBuilder();
		b.append(stab).append("<").append(key).append(">");
		b.append(value);
		b.append("</").append(key).append(">\n");
		return (b.toString());
	}

	public static String getClean(Integer d) {
		return (d != null ? d.toString() : "");
	}

	public static String getClean(Long d) {
		return (d != null ? d.toString() : "");
	}

	public static String getClean(String str) {
		return (str != null ? str : "");
	}

}

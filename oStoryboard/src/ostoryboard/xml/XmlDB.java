/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.xml;

import ostoryboard.i18n.I18Nmsg;
import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author FaVdB
 */
public class XmlDB {
	public static final I18Nmsg i18n = I18Nmsg.getInstance();

	public static String getTitle(String val) {
		XmlDB xml = new XmlDB(val);
		if (xml.open() == false) {
			return (null);
		}
		Node n = xml.findNode("info");
		String t = attributeGetString(n, "title");
		xml.close();
		return (t);
	}

	public String name;
	public String title="";
	private final File file;
	private boolean opened = false;
	private boolean modified = false;
	public Document document;
	public Element rootNode;
	public DocumentBuilder documentBuilder;

	public XmlDB(String path) {
		name = path;
		file = new File(path);
	}

	private static void trace(String msg) {
		System.out.println(msg);
	}

	private static void error(String msg, Exception ex) {
		System.err.println(msg);
		ex.printStackTrace(System.err);
	}

	private void init() {
		if (document == null) {
			try {
				documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				document = documentBuilder.newDocument();
			} catch (ParserConfigurationException ex) {
				error("xml.err.create", ex);
			}
		}
		rootNode = document.createElement("book");
		document.appendChild(rootNode);
	}

	public boolean isOK() {
		boolean rc = true;
		File f = new File(name);
		// file doesn't exist
		if (!f.exists()) {
			String txt = i18n.str("file.notexists")+"\n"+ f.getPath();
			JOptionPane.showMessageDialog(null, txt,
					i18n.str("z.warning"),
					JOptionPane.ERROR_MESSAGE);
			rc = false;
		}
		// file is read-only
		if (!f.canWrite()) {
			String txt = i18n.str("xml.readonly")+"\n"+ f.getPath();
			JOptionPane.showMessageDialog(null, txt,
					i18n.str("z.warning"),
					JOptionPane.ERROR_MESSAGE);
			rc = false;
		}
		return (rc);
	}

	public boolean isOpened() {
		return (opened);
	}

	public static XmlDB create(String fileName, String title, boolean b) {
		if (fileName == null || fileName.isEmpty()) {
			return (null);
		}
		String fname = fileName;
		if (!fileName.endsWith(".osbd")) {
			fname = fileName + ".osbd";
		}
		XmlDB xml = new XmlDB(fname);
		xml.init();
		xml.setTitle(title);
		xml.save(true);
		xml.close();
		return (xml);
	}

	public static boolean rename(String source, String dest) {
		if (source == null || source.isEmpty()
				|| dest == null || dest.isEmpty()) {
			return (false);
		}
		File inf = new File(source);
		File ouf = new File(dest);
		if (inf.exists() && !ouf.exists()) {
			inf.renameTo(ouf);
		}
		return (true);
	}

	public static boolean delete(String source) {
		if (source == null || source.isEmpty()) {
			return (false);
		}
		File inf = new File(source);
		return (inf.delete());
	}

	public static boolean copy(String source, String dest) {
		if (source == null || source.isEmpty()
				|| dest == null || dest.isEmpty()) {
			return (false);
		}
		File inf = new File(source);
		if (inf.exists()) {
			InputStream is;
			OutputStream os;
			try {
				is = new FileInputStream(source);
				os = new FileOutputStream(dest);
				byte[] buffer = new byte[1024];
				int length;
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
				is.close();
				os.close();
			} catch (FileNotFoundException ex) {
				error("file " + source + " not found", ex);
				return (false);
			} catch (IOException ex) {
				error("unable to copy file " + source + " to " + dest, ex);
				return (false);
			}
		}
		return (true);
	}

	public boolean open() {
		if (isOpened()) {
			return (true);
		}
		documentBuilder = null;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			error("DocumentBuilder error: ", ex);
			return (false);
		}
		document = readDom();
		if (document == null) {
			rootNode = null;
			return (false);
		}
		rootNode = document.getDocumentElement();
		opened = true;
		resetModified();
		return (true);
	}

	public void close() {
		if (opened) {
			opened = false;
			resetModified();
			document = null;
			documentBuilder = null;
		}
	}

	public Document readDom() {
		Document rc = null;
		try {
			rc = documentBuilder.parse(new File(name));
		} catch (IOException | SAXException e) {
			error("XmlDB.readDom() Parsing error for " + name, e);
		}
		return (rc);
	}

	public Node findNode(String typeobj) {
		NodeList nodes = rootNode.getElementsByTagName(typeobj);
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!n.getParentNode().equals(rootNode)) {
				continue;
			}
			if (n.getNodeName().equals(typeobj)) {
				return (n);
			}
		}
		return (null);
	}

	public Node findNode(Node node, String name) {
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!n.getParentNode().equals(node)) {
				continue;
			}
			if (n.getNodeName().equals(name)) {
				return (n);
			}
		}
		return (null);
	}

	public Node findNode(String tobj, String name) {
		NodeList nodes = document.getElementsByTagName(tobj);
		try {
			if (nodes.getLength() == 0) {
				return (null);
			}
		} catch (NullPointerException ex) {
			return (null);
		}
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!n.getParentNode().equals(rootNode)) {
				continue;
			}
			if (n.getNodeName().equals(tobj)) {
				String x = attributeGetString(n, "name");
				if (x.equals(name)) {
					return (n);
				}
			}
		}
		return (null);
	}

	public boolean reformat(String str) {
		close();
		try {
			FileWriter fw;
			fw = new FileWriter(name, false);
			try (BufferedWriter bw = new BufferedWriter(fw)) {
				bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
				bw.write("<storyboard title=\""+title+"\">\n");
				bw.write(str);
				bw.write("</storyboard>");
				bw.flush();
				bw.close();
			}
		} catch (IOException ex) {
			error("XmlDB.reformat(str=" + str + ") error", ex);
			return (false);
		}
		open();
		return (true);
	}

	/**
	 * enregistrement du fichier XML en cours
	 *
	 * @param projectName: nom du projet
	 * @param force : vrai si l'enregistrement est à faire même si le fichier n'a pas été modifié, sinon si le fichier
	 * n'a pas été modifié on ne fait rien.
	 *
	 * @return vrai si l'opération s'est correctement déroulée.
	 */
	public boolean save(boolean force) {
		trace("XmlDB.save(force=" + (force ? "true" : "false") + ")");
		if (!force && !isModified()) {
			return (true);
		}
		rootNode.setAttribute("title", title);
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new File(name));
			transformer.transform(source, result);
		} catch (TransformerConfigurationException ex) {
			error("Xml.save() exception", ex);
			return (false);
		} catch (TransformerException ex) {
			error("Xml.save() exception", ex);
			return (false);
		}
		resetModified();
		return (true);
	}

	/**
	 * marquer le fichier XML comme modifié
	 *
	 */
	public void setModified() {
		modified = true;
	}

	/**
	 * réinitialisation de l'indicateur de modification du fichier XML
	 *
	 */
	public void resetModified() {
		modified = false;
	}

	/**
	 * le fichier XML a-t-il été modifié
	 *
	 * @return
	 */
	public boolean isModified() {
		return (modified);
	}
	
	public void setTitle(String title) {
		this.title=title;
	}

	/**
	 * set a String value to an attribut
	 *
	 * @param node : node
	 * @param attribute : attribut
	 * @param val : a String value
	 */
	public static void attributeSet(Node node, String attribute, String val) {
		if (node == null || attribute == null || attribute.isEmpty()) {
			return;
		}
		Element e = (Element) node;
		if (e.getAttribute(attribute) != null) {
			if (val != null && !val.isEmpty()) {
				e.setAttribute(attribute, val);
			} else {
				e.removeAttribute(attribute);
			}
			return;
		}
		if (val == null || val.isEmpty()) {
			return;
		}
		Node nv = node.getAttributes().getNamedItem(attribute);
		if (nv != null) {
			nv.setNodeValue(val);
		}
	}

	/**
	 * set a Long value to an attribute
	 *
	 * @param node
	 * @param attribute
	 * @param val : the Long value
	 */
	public static void attributeSet(Node node, String attribute, Long val) {
		if (val != null) {
			attributeSet(node, attribute, Long.toString(val));
		} else {
			attributeRemove(node, attribute);
		}
	}

	/**
	 * set an Integer value to an attribut
	 *
	 * @param node
	 * @param attribute
	 * @param val : the Integer value
	 */
	public static void attributeSet(Node node, String attribute, Integer val) {
		if (val != null) {
			attributeSet(node, attribute, val.toString());
		} else {
			attributeRemove(node, attribute);
		}
	}

	/**
	 * set a Boolean value to an attribut
	 *
	 * @param node
	 * @param attribute
	 * @param val : the Boolean value
	 */
	public static void attributeSet(Node node, String attribute, Boolean val) {
		if (val != null) {
			attributeSet(node, attribute, val.toString());
		} else {
			attributeRemove(node, attribute);
		}
	}

	/**
	 * remove an attribte if it exists
	 *
	 * @param node
	 * @param attribute
	 */
	public static void attributeRemove(Node node, String attribute) {
		Element e = (Element) node;
		if (e == null) {
			return;
		}
		if (e.getAttribute(attribute) != null) {
			e.removeAttribute(attribute);
		}
	}

	/**
	 * return a child content as a String
	 *
	 * @param node : parent node
	 * @param child : name of the child node to search
	 * @return : a String, if the child is not found return an empty String
	*
	 */
	public static String childContentGet(Node node, String child) {
		NodeList t = node.getChildNodes();
		if (t.getLength() > 0) {
			for (int i = 0; i < t.getLength(); i++) {
				if (t.item(i).getNodeName().equals(child)) {
					return (t.item(i).getTextContent().trim());
				}
			}
		}
		return ("");
	}

	/**
	 * record data of a child node, rules :
	 * - if the value is empty or nulll the child node is remove
	 * - if the child node doesn't exists it is created
	 *
	 * @param doc : the XML document
	 * @param node : parent node
	 * @param key : name of the child node, forced lowercase
	 * @param val : value to record
	 */
	public static void childContentSet(Document doc, Node node, String key, String val) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(key.toLowerCase())) {
				if (val == null || val.isEmpty()) {
					node.removeChild(n);
				} else {
					n.setNodeValue(val);
				}
				return;
			}
		}
		if (val == null || val.isEmpty()) {
			return;
		}
		Element e = doc.createElement(key.toLowerCase());
		e.setNodeValue(val);
		node.appendChild(e);
	}

	/**
	 * get an Integer value for an attribut or the default value if not exists
	 *
	 * @param node
	 * @param attribute
	 * @param def : default value, optional
	 * @return the Integer value or the default value
	 */
	public static Integer attributeGetInteger(Node node, String attribute, int... def) {
		String x = attributeGetString(node, attribute);
		if (x.isEmpty() || "null".equals(x)) {
			if (def != null && def.length > 0) {
				return (def[0]);
			}
			return (0);
		}
		return (Integer.parseInt(x));
	}

	/**
	 * get a list of Strings from an attribute
	 *
	 * @param node
	 * @param attribute
	 * @return the list of strings
	 */
	public static List<String> attributeGetList(Node node, String attribute) {
		String str = attributeGetString(node, attribute);
		List<String> list = new ArrayList<>(Arrays.asList(str.split(",")));
		return (list);
	}

	/**
	 * get a Long value for an attribut, a default value if it's missing
	 *
	 * @param node
	 * @param attribute
	 * @param def : défault value if missing, optional
	 * @return the Long, or the default value
	 */
	public static Long attributeGetLong(Node node, String attribute, long... def) {
		String x = attributeGetString(node, attribute);
		if (x.isEmpty() || "null".equals(x) || !XmlUtil.isLong(x)) {
			if (def != null && def.length > 0) {
				return (def[0]);
			}
			return (-1L);
		}
		return (Long.parseLong(x));
	}

	/**
	 * get a String value for an attribute
	 *
	 * @param node
	 * @param attribut
	 * @param def : default value if is missing, optional
	 * @return
	 */
	public static String attributeGetString(Node node, String attribut, String... def) {
		String val = "";
		if (def != null && def.length > 0) {
			val = def[0];
		}
		if (node != null) {
			val = ((Element) node).getAttribute(attribut);
		}
		return (val.trim());
	}

	/**
	 * set a String value for an attribute
	 *
	 * @param node
	 * @param attribute
	 * @param val : the String value to set
	 *
	 * Note : if the node doesn't exist it is created
	 */
	public void attributeSetValue(Node node, String attribute, String val) {
		Node n = node.getAttributes().getNamedItem(attribute);
		String cleanval = XmlUtil.getClean(val);
		if (n != null) {
			n.setNodeValue(cleanval);
		} else {
			Element e = (Element) node;
			e.setAttribute(attribute, cleanval);
		}
	}

	/**
	 * set a Long value to an attributs
	 *
	 * @param node
	 * @param attribute
	 * @param val : the Long value
	 */
	public void attributeSetValue(Node node, String attribute, Long val) {
		String cleanval = XmlUtil.getClean(val);
		attributeSetValue(node, attribute, cleanval);
	}

	/**
	 * set an Integer to an attribute
	 *
	 * @param node
	 * @param attribute
	 * @param val : the Integer value
	 */
	public void attributeSetValue(Node node, String attribute, int val) {
		String cleanval = XmlUtil.getClean(val);
		attributeSetValue(node, attribute, cleanval);
	}

	/**
	 * set a Boolean value to an attribute
	 *
	 * @param node
	 * @param attribute
	 * @param val : the Boolean value
	 */
	public void attributeSetValue(Node node, String attribute, Boolean val) {
		attributeSetValue(node, attribute, (val ? "1" : "0"));
	}

	/**
	 * set a Dimension value to an attribute
	 *
	 * @param node 
	 * @param attribute
	 * @param val : the Dimension value, sets "0,0" if null
	 */
	public void attributeSetValue(Node node, String attribute, Dimension val) {
		if (val==null) {
			attributeSetValue(node, attribute, "0,0");
		} else 
			attributeSetValue(node, attribute, String.format("%d,%d", val.height, val.width));
	}

	/**
	 * set a String value to a child node
	 *
	 * @param xml : XmlDB file
	 * @param node : parent node
	 * @param child : child node
	 * @param val : tha String value, if is empty and child node exists it is removed
	 */
	public void childContentSet(XmlDB xml, Node node, String child, String val) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(child)) {
				if (val == null || val.isEmpty()) {
					node.removeChild(n);
				} else {
					n.setNodeValue(val);
				}
				return;
			}
		}
		Element e = xml.document.createElement(child);
		e.setNodeValue(val);
		node.appendChild(e);
	}

	/**
	 * get a String value from a child node
	 *
	 * @param node : parent node
	 * @param child : name of the child node
	 * @return : th String value, may be empty if child doesn't exist
	 *
	 * Note : if child node doesn't exist it is created
	 */
	public static Node childGetNode(Node node, String child) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(child)) {
				return (n);
			}
		}
		Node n = node.getOwnerDocument().createElement(child);
		node.appendChild(n);
		return (n);
	}

	/**
	 * get a String value from an existing child node
	 *
	 * @param node :parent node
	 * @param child : name of the child node
	 * @return : the String value, null if child node doesn't exist
	 */
	public static Node childGetExisting(Node node, String child) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(child)) {
				return (n);
			}
		}
		return (null);
	}

	/**
	 * get an object File of the opened XML
	 *
	 * @return : the File
	 */
	public File getFile() {
		return (file);
	}

	/**
	 * get the Path of the opened XML
	 *
	 * @return : the Path
	 */
	public Path getFilePath() {
		return Paths.get(file.getAbsolutePath());
	}

	/**
	 * get the path stringof the opened XML
	 *
	 * @return : the path String
	 */
	public String getPath() {
		return (file.getPath());
	}

	public String getOnlyPath() {
		String str = file.getPath();
		str = str.substring(0, str.lastIndexOf(File.separator));
		return (str);
	}

	/**
	 * convert the today date in a String
	 *
	 * @return : in formatted date with "yyyyMMddHHmmss"
	 */
	public static String getDateString() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		return (formatter.format(new Date()));
	}

	/**
	 * do a backup file of the opened XML
	 *
	 * @param toFilename : name of the destinationfile
	 * @param incremental : if true add the formatted today date to the file name
	 */
	public void doBackup(String toFilename, boolean incremental) {
		String backup = toFilename;
		if (incremental) {
			backup += "_" + getDateString();
		}
		backup += ".backup";
		Path toPath = Paths.get(backup);
		try {
			Files.copy(getFilePath(), toPath);
		} catch (IOException ex) {
			error("Error XmlDB.doBackup to " + backup, ex);
		}
		System.out.println("Backup file to " + backup);
	}

	/**
	 * restore a previous backup file
	 *
	 * @param file : the File to restore
	 */
	public void doRestore(File file) {
		Path fromPath=Paths.get(file.getAbsolutePath());
		try {
			Files.copy(fromPath, getFilePath());
		} catch (IOException ex) {
			error("Error XmlDB.doRestore from " + file.getAbsolutePath(), ex);
		}
	}

	/**
	 * remove a node
	 *
	 * @param str: name node to remove
	 */
	public void removeNode(String str) {
		Node node = findNode(str);
		if (node != null) {
			rootNode.removeChild(node);
		}
		setModified();
	}

}

/*
 * Copyright (C) oStorybook Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * inspired by novaworx API
 * adaptation favdb
 */
package ostoryboard.util;

public final class Log {

	private static int LEVEL = 4;
	public static final int
			NONE = 0,
			DEVEL = 1,
			DEBUG = 2,
			CONFIG = 3,
			ERROR = 4,
			ALL=5;

	public static void setLevel(int level) {
		LEVEL = level;
		String str[]={"NONE","DEVEL","DEBUG","CONFIG","ERROR","ALL"};
		trace("Log level setting to "+str[level]);
	}

	public static void write(int priority, Throwable t) {
		if (priority == ERROR) {
			t.printStackTrace(System.err);
		} else if (priority<=LEVEL) {
			t.printStackTrace(System.out);
		}
	}

	public static void trace(String text) {
		write(1, text);
	}

	public static void message(String text) {
		write(2, text);
	}

	public static void write(int priority, String text) {
		if (priority == ERROR) {
			System.err.println(text);
		} else if (priority<=LEVEL) {
			System.out.println(text);
		}
	}
}

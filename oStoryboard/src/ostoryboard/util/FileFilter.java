package ostoryboard.util;

import java.io.File;
import ostoryboard.i18n.I18Nmsg;

public class FileFilter extends javax.swing.filechooser.FileFilter {
	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	public static String 
		HTML[] = {".htm", ".html"},
		IMG[] = {".jpeg", ".jpg", ".png", ".gif"},
		PNG = ".png",
		ODT = ".odt",
		XML = ".xml";

	private String extension;
    private String[] extensions;
	private String description;
	
	public FileFilter(String ext, String desc) {
		extension=ext;
		description=desc;
	}

	public FileFilter(String ext) {
		extension=ext;
		description=i18n.str("file.type.h2");
	}

	public FileFilter(String[] extensions, String description) {
        this.extensions = extensions;
        this.description = description;
    }
	
	public FileFilter(String[] extensions) {
        this.extensions = extensions;
		switch(extensions[0]) {
			case ".jpeg":
			case ".jpg":
			case ".gif":
				description=i18n.str("file.type.img");break;
			case ".odt": description=i18n.str("file.type.odt");break;
			case ".png": description=i18n.str("file.type.png");break;
			case ".xml": description=i18n.str("file.type.xml");break;
			default: description=i18n.str("file.type.unknown");break;

		}
    }
	
	@Override
    public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		if (extension==null) {
			for (String x:extensions) {
				if (file.getName().endsWith(x)) return(true);
			}
			return(false);
		}
        return file.getName().endsWith(extension);
    }
	@Override
    public String getDescription() {
        return description;
    }
}

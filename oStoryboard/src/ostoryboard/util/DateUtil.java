/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author favdb
 */
public class DateUtil {

	public static String getDate() {
		Date date = new Date();
		DateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		return(f.format(date));
	}

	public static String getDate(Date date) {
		DateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		return(f.format(date));
	}

}

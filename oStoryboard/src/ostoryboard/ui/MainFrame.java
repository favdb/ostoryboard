/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ostoryboard.ui;

import ostoryboard.i18n.I18Nmsg;
import ostoryboard.Pref;
import ostoryboard.actions.MainActions;
import ostoryboard.dialog.NewFileDlg;
import ostoryboard.model.Scene;
import ostoryboard.oStoryboard;
import ostoryboard.ui.panel.ScenarioPanel;
import ostoryboard.ui.panel.StoryboardPanel;
import ostoryboard.util.VerticalLabelUI;
import ostoryboard.xml.XmlDB;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ostoryboard.util.Log;

/**
 *
 * @author favdb
 */
public class MainFrame extends JFrame implements ActionListener {

	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	private MainActions mainActions;
	public MainMenu mainMenu;
	private StatusBar statusbar;
	private StoryboardPanel storyboard;
	private ScenarioPanel scenario;
	private List<Scene> scenes = new ArrayList<>();
	private JTabbedPane tabbed;
	private Scene scene;
	private XmlDB xml;
	private int curScene;
	private boolean modified;
	private String projectName;

	public MainFrame() {
		super();
		initAll();
	}

	private void initAll() {
		init();
		initUi();
	}

	private void init() {
		this.setTitle("oStoryboard");
		mainActions = new MainActions(this);
		scenes = new ArrayList<>();
		scene = new Scene(1, 1);
		scenes.add(scene);
		curScene = 0;
		String fname = oStoryboard.pref.getString(Pref.KEY.LAST_FILE);
		if (!fname.isEmpty()) {
			xml = new XmlDB(fname);
			scenes = loadScenes();
			if (!scenes.isEmpty()) {
				scene = scenes.get(0);
				curScene = 0;
			} else {
				scenes.add(scene);
			}
		}
		mainMenu = new MainMenu(this);
		statusbar = new StatusBar(this);
		storyboard = new StoryboardPanel(this, scene);
		scenario = new ScenarioPanel(this, scene);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	private void initUi() {
		this.setLayout(new BorderLayout());
		this.setJMenuBar(mainMenu.getMenubar());
		JToolBar tool = mainMenu.getToolbar();
		JButton bt = new JButton("check");
		bt.addActionListener(e -> {
			storyboard.getData();
		});
		tool.add(bt);
		this.add(mainMenu.getToolbar(), BorderLayout.NORTH);
		tabbed = new JTabbedPane(JTabbedPane.RIGHT);
		tabbed.add("Storyboard", storyboard);
		JLabel lab1 = new JLabel(" Storyboard ");
		lab1.setUI(new VerticalLabelUI(false));
		tabbed.setTabComponentAt(0, lab1);
		tabbed.add(" Scenario ", scenario);
		JLabel lab2 = new JLabel(" Scenario ");
		lab2.setUI(new VerticalLabelUI(false));
		tabbed.setTabComponentAt(1, lab2);
		this.add(tabbed, BorderLayout.CENTER);
		this.add(statusbar, BorderLayout.SOUTH);
		this.setPreferredSize(new Dimension(800, 632));
		this.pack();
		this.setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		mainActions.exec(e);
	}

	public void exit() {
		dispose();
		System.exit(0);
	}

	public void addScene() {
		Log.trace("MainFrame.addScene()");
		if (storyboard.isModified()) {
			storyboard.apply();
		}
		Scene s = new Scene(scene.seq, scene.plan + 1);
		scenes.add(s);
		scenes.sort(Comparator.comparing(Scene::getName));
		curScene = scenes.size() - 1;
		scene = scenes.get(curScene);
		mainMenu.refeshScenesTB(s.getName());
		storyboard.setScene(s);
	}

	public List<Scene> getScenes() {
		return scenes;
	}

	public void setModified() {
		this.modified = true;
		this.setTitle("*oStoryboard*");
	}

	public void resetModified() {
		this.modified = false;
		this.setTitle("oStoryboard");
	}

	public void createFile() {
		if (askModified()) {
			saveFile();
		}
		xml = null;
		scenes = new ArrayList<>();
		scene = new Scene(1, 1);
		scenes.add(scene);
		curScene = 0;
		storyboard.setScene(scene);
	}

	public void openFile() {
		if (askModified()) {
			if (askForSave() == false) {
				return;
			}
			saveFile();
		}
		String fname = selectFileName();
		if (fname.isEmpty()) {
			return;
		}
		XmlDB xf = new XmlDB(fname);
		if (xf.open()) {
			xml = xf;
		}
	}

	public void saveFile() {
		if (xml == null || xml.name.isEmpty()) {
			if (askFileName() == false) {
				return;
			}
		}
		if (xml != null) {
			Scene sc = storyboard.apply();
			scenes.set(curScene, sc);
			StringBuilder b = new StringBuilder();
			for (Scene s : scenes) {
				b.append(s.toXml());
			}
			xml.reformat(b.toString());
		}
	}

	private boolean askModified() {
		return storyboard.isModified();
	}

	private boolean askForSave() {
		Object[] choix = {
			i18n.str("cancel"),
			i18n.str("file.save"),
			i18n.str("ignore"),};
		int n = JOptionPane.showOptionDialog(null,
				i18n.str("file.ask_for_save"),
				i18n.str("file.save"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null, choix, choix[0]);
		return n == 1;
	}

	public boolean askFileName() {
		NewFileDlg dlg = new NewFileDlg(this, true);
		dlg.setVisible(true);
		if (dlg.canceled || dlg.getFile() == null) {
			return false;
		}
		projectName = dlg.getProjectName();
		xml = new XmlDB(dlg.getFile().getAbsolutePath());
		return true;
	}

	public String selectFileName() {
		Log.trace("MainFrame.selectFileName()");
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int ret = fc.showOpenDialog(this);
		if (ret != JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile().getAbsolutePath();
		}
		return "";
	}

	private List<Scene> loadScenes() {
		List<Scene> list = new ArrayList<>();
		NodeList nodes = xml.document.getElementsByTagName("scene");
		try {
			if (nodes.getLength() != 0) {
				for (int i = 0; i < nodes.getLength(); i++) {
					Node n = nodes.item(i);
					list.add(new Scene(n));
				}
			}
		} catch (NullPointerException ex) {
			ex.printStackTrace(System.err);
		}
		return list;
	}

	public void setScene(String str) {
		Log.trace("MainFrame.setScene(str=" + str + ")");
		if (storyboard.isModified()) {
			storyboard.apply();
		}
		for (int i = 0; i < scenes.size(); i++) {
			if (scenes.get(i).getName().equals(str)) {
				curScene = i;
				scene = scenes.get(i);
				storyboard.setScene(scene);
			}
		}
	}

	public void setScene(int p) {
		int i = mainMenu.cbScenes.getSelectedIndex();
		switch (p) {
			case -1: //previous scene
				curScene--;
				mainMenu.cbScenes.setSelectedIndex(i - 1);
				break;
			case 0: //first scene
				curScene=0;
				mainMenu.cbScenes.setSelectedIndex(0);
				break;
			case 1: //next scene
				mainMenu.cbScenes.setSelectedIndex(i + 1);
				curScene++;
				if (curScene == scenes.size()) {
					curScene--;
				}
				break;
			case 2: //last scene
				mainMenu.cbScenes.setSelectedIndex(mainMenu.cbScenes.getItemCount()-1);
				curScene = scenes.size() - 1;
				break;
		}
		if (curScene<0) curScene=0;
		if (curScene>=scenes.size()-1) curScene=scenes.size()-1;
		storyboard.setScene(scenes.get(curScene));
	}

}

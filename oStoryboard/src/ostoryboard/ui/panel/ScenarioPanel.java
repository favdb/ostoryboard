/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.ui.panel;

import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import net.miginfocom.swing.MigLayout;
import ostoryboard.model.Scene;
import ostoryboard.model.Codes;
import ostoryboard.ui.MainFrame;
import ostoryboard.util.Markdown;

/**
 *
 * @author favdb
 */
public class ScenarioPanel extends AbstractPanel {

	private Scene scene;
	private boolean changed;
	private JTextField tfName;
	private JTextField tfNumber;
	private JTextField tfDuration;
	private JTextField tfPersons;
	private JTextField tfPitch;
	private JComboBox cbStatus,
			cbMoment,
			cbFocal,
			cbStart,
			cbEnd,
			cbStage;
	private Markdown mText;

	public ScenarioPanel(MainFrame mainFrame, Scene scene) {
		super(mainFrame);
		this.scene=scene;
	}

	@Override
	public void initialize() {
		setLayout(new MigLayout("fill, ins 1 1 1 1"));
		JTabbedPane tb = new JTabbedPane();
		JPanel commun = new JPanel(new MigLayout("hidemode 2, wrap 2, ins 2", "[][]"));
		tfName = initField("name", 28, "");
		JPanel p1 = new JPanel(new MigLayout());
		p1.add(tfName);
		p1.add(new JLabel(i18n.strColon("scene.number")));
		tfNumber = initField("number", 4, "");
		p1.add(tfNumber);
		setRow(commun, "name", p1, "span, growx");
		tb.add(i18n.str("scene.common"), commun);
		add(tb, "grow");
		cbStatus = this.initCombo("status", Codes.getStatusList(), true);
		setRow(commun, "status", cbStatus);
		tfDuration = initField("duration", 3, "");
		tfPersons = initField("persons", 32, "");
		tfPersons.setEditable(false);
		setRow(commun, "scene.persons", tfPersons);
		tfPitch = initField("pitch", 32, "");
		setRow(commun, "scene.pitch", tfPitch);
		JPanel p2 = new JPanel(new MigLayout());
		cbMoment = initCombo("moment", Codes.getLightList(), true);
		p2.add(cbMoment);
		p2.add(new JLabel(i18n.strColon("scene.focal")));
		cbFocal = initCombo("focal", Codes.getFocaleList(), true);
		p2.add(cbFocal);
		p2.add(new JLabel(i18n.strColon("scene.start")));
		cbStart = initCombo("start", Codes.getStartList(),true);
		p2.add(cbStart);
		p2.add(new JLabel(i18n.strColon("scene.end")));
		cbEnd = initCombo("end", Codes.getEndList(),true);
		p2.add(cbEnd);
		setRow(commun, "scene.moment", p2);
		cbStage = this.initCombo("stage", Codes.getStageList(), true);
		setRow(commun, "scene.stage", cbStage);
		mText = new Markdown("");
		mText.setCaretPosition(0);
		tb.add(i18n.str("scene.text"), mText);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void stateChanged(ChangeEvent e) {
	}

	public void setScene(Scene scene) {
		this.scene = scene;
		changed = false;
		refresh();
	}

	@Override
	public void refresh() {
		if (scene != null) {
			tfName.setText(scene.getName());
			mText.setText(scene.getText());
		}
	}

}

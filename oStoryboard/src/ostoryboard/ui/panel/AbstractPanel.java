/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.ui.panel;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.event.ChangeListener;
import ostoryboard.i18n.I18Nmsg;
import ostoryboard.resources.images.Images;
import ostoryboard.ui.MainFrame;
import javax.swing.JButton;

/**
 *
 * @author favdb
 */
public abstract class AbstractPanel extends JPanel implements ActionListener, ChangeListener {

	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	public final MainFrame mainFrame;
	public JToolBar toolbar;

	public AbstractPanel(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		initialize();
	}

	public abstract void initialize();

	public JToolBar initToolbar() {
		toolbar = new JToolBar();
		return toolbar;
	}

	public void refresh() {

	}

	public JCheckBox initCheckImage(String name) {
		JCheckBox ck = new JCheckBox();
		ck.setName(name);
		ck.setToolTipText(i18n.str(name));
		ck.setIcon(Images.getIcon(name));
		ck.addActionListener(this);
		return ck;
	}

	public JRadioButton initRadioImage(String name) {
		JRadioButton rb = new JRadioButton();
		rb.setName(name);
		rb.setToolTipText(i18n.str(name));
		rb.setIcon(Images.getIcon(name));
		rb.addChangeListener(this);
		return rb;
	}

	public JToggleButton initToggleImage(String name, int size) {
		JToggleButton tb = new JToggleButton(Images.getIcon(name, size));
		tb.setName(name);
		tb.setToolTipText(i18n.str(name));
		tb.addActionListener(this);
		return tb;
	}

	public JToggleButton initToggleText(String name, String text, boolean tools) {
		JToggleButton tb = new JToggleButton(text);
		tb.setName(name);
		if (tools) tb.setToolTipText(i18n.str(name));
		tb.addActionListener(this);
		return tb;
	}

	public JTextField initField(String name, int len, String in) {
		JTextField tf = new JTextField();
		tf.setName("name");
		tf.setText(in);
		if (len>0) tf.setColumns(len);
		return tf;
	}

	@SuppressWarnings("unchecked")
	public JComboBox initCombo(String name, List list, boolean mini) {
		JComboBox cb=new JComboBox();
		cb.setName(name);
		Dimension sz=cb.getMinimumSize();
		sz.width=sz.height*4;
		if (!mini) cb.setPreferredSize(sz);
		for (Object obj:list) {
			cb.addItem(obj);
		}
		cb.addActionListener(this);
		return cb;
	}
	
	public JButton initButton(String name, String text, String icon) {
		JButton bt=new JButton();
		if (!text.isEmpty()) {
			bt.setText(text);
		}
		if (!icon.isEmpty()) {
			bt.setIcon(Images.getIcon(icon));
		}
		bt.setToolTipText(i18n.str(name));
		bt.addActionListener(this);
		return bt;
	}
	
	public void setRow(JPanel panel, String key, JComponent component, String... grow) {
		panel.add(new JLabel(i18n.strColon(key)),"right");
		if (grow.length > 0) {
			panel.add(component, grow[0]);
		} else {
			panel.add(component);
		}
	}

}

/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.ui.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import net.miginfocom.swing.MigLayout;
import ostoryboard.dialog.ImageChooserDlg;
import ostoryboard.model.Scene;
import ostoryboard.resources.images.Images;
import ostoryboard.ui.MainFrame;
import ostoryboard.util.Log;
import java.awt.Component;

/**
 *
 * @author favdb
 */
public class StoryboardPanel extends AbstractPanel implements ChangeListener {
	
	private Scene scene = new Scene(1,1);
	private JTextField tfSeq;
	private JPanel pTopLeft;
	private JTextField tfPlan;
	private JTextField tfDur;
	private JPanel pTopRight;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JPanel pImage;
	private JTextArea tfNotes;
	private JButton in_ex;
	private JToggleButton shot_normal, shot_high, shot_low;
	private JButton btImage;
	private int originalHash;
	
	public StoryboardPanel(MainFrame mainFrame, Scene scene) {
		super(mainFrame);
		setScene(scene);
	}
	
	@Override
	public void initialize() {
		this.removeAll();
		setLayout(new MigLayout("fill, ins 1, wrap", "[][fill][]"));
		// en haut à gauche: numéros de séquence et de plan, durée
		JPanel pTop = new JPanel(new MigLayout("fill, ins 0"));
		pTopLeft = new JPanel(new MigLayout("fill, ins 1"));
		pTopLeft.add(new JLabel(i18n.strColon("scene.seq")));
		tfSeq = new JTextField();
		tfSeq.setColumns(3);
		pTopLeft.add(tfSeq);
		pTopLeft.add(new JLabel(i18n.strColon("scene.shot")));
		tfPlan = new JTextField();
		tfPlan.setColumns(3);
		pTopLeft.add(tfPlan);
		pTopLeft.add(new JLabel(i18n.strColon("scene.duration")));
		tfDur = new JTextField();
		tfDur.setColumns(3);
		pTopLeft.add(tfDur);
		pTop.add(pTopLeft,"growx");
		// en haut à droite : cadrage (normal, plongée, contre-plongée
		pTopRight = new JPanel(new MigLayout("fill, ins 1"));
		ButtonGroup bg = new ButtonGroup();
		shot_normal = initToggleImage("shot.normal", 64);
		bg.add(shot_normal);
		pTopRight.add(shot_normal, "sg shot");
		shot_high = initToggleImage("shot.high", 64);
		bg.add(shot_high);
		pTopRight.add(shot_high, "sg shot");
		shot_low = initToggleImage("shot.low", 64);
		bg.add(shot_low);
		pTopRight.add(shot_low, "sg shot");
		pTop.add(pTopRight);
		add(pTop, "span, grow");
		// à gauche barre verticale: focus (de 12 à 135)
		leftPanel = new JPanel(new MigLayout("gapy 0, wrap, ins 0 0 0 0", "[center]"));
		leftPanel.add(new JLabel("FOC."));
		ButtonGroup bg2 = new ButtonGroup();
		int foc[] = {12, 24, 35, 50, 70, 85, 100, 135};
		for (int i : foc) {
			JToggleButton tb = initToggleText("foc." + i, i + "", false);
			bg2.add(tb);
			leftPanel.add(tb,"sg foc");
		}
		add(leftPanel, "top, spany");
		// au centre: esquisse dessinée
		pImage = new JPanel(new MigLayout("fill"));
		pImage.setBackground(Color.white);
		pImage.setMinimumSize(new Dimension(320, 240));
		add(pImage, "grow");
		// à droite barre verticale: en haut Mouvement (7 valeurs), en bas Jour/Nuit
		rightPanel = new JPanel(new MigLayout("gapy 0, wrap, ins 0", "[center]"));
		rightPanel.add(new JLabel("MVT."));
		String mvt = "FPTSGZD";
		//ButtonGroup bg3 = new ButtonGroup();
		for (int i = 0; i < mvt.length(); i++) {
			JToggleButton tb = initToggleText("mvt." + mvt.charAt(i), mvt.charAt(i) + "", true);
			//bg3.add(tb);
			rightPanel.add(tb, "sg right");
		}
		rightPanel.add(new JLabel(" "), "growy");
		ButtonGroup bg4 = new ButtonGroup();
		JToggleButton tb1 = initToggleImage("light.day", 20);
		bg4.add(tb1);
		rightPanel.add(tb1, "sg right");
		JToggleButton tb2 = initToggleImage("light.night", 20);
		bg4.add(tb2);
		rightPanel.add(tb2, "sg right");
		
		add(rightPanel, "top, wrap");
		// en bas Notes sur 3 lignes maxi, à gauche INT/EXT
		tfNotes = new JTextArea();
		tfNotes.setLineWrap(true);
		tfNotes.setRows(3);
		add(tfNotes, "skip 1, grow");
		in_ex = new JButton("INT");
		in_ex.setName("in_ex");
		in_ex.setToolTipText("INT./EXT.");
		in_ex.addActionListener(this);
		add(in_ex, "top, right");
		refresh();
	}
	
	@Override
	public void refresh() {
		if (scene==null) return;
		tfSeq.setText(scene.getSeq().toString());
		tfPlan.setText(scene.getPlan().toString());
		tfDur.setText(scene.getDuration().toString());
		switch (scene.getShot()) {
			case 1:
				shot_normal.setSelected(true);
				break;
			case 2:
				shot_high.setSelected(true);
				break;
			case 3:
				shot_low.setSelected(true);
				break;
		}
		String focal = scene.getFocal().toString();
		if (scene.getFocal() > 0) {
			for (Component comp : leftPanel.getComponents()) {
				if (comp instanceof JToggleButton) {
					JToggleButton tb = (JToggleButton) comp;
					tb.setSelected(false);
					if (tb.getName().startsWith("foc.") && focal.equals(tb.getText())) {
						tb.setSelected(true);
					}
				}
			}
		}
		refreshImage(scene.getImg());
		int light = scene.getMoment();
		String mvt = scene.getMvt();
		for (Component comp : rightPanel.getComponents()) {
			if (comp instanceof JToggleButton) {
				JToggleButton tb = (JToggleButton) comp;
				tb.setSelected(false);
				if (tb.getName().equals("light.day") && light == 1) {
					tb.setSelected(true);
				} else if (tb.getName().equals("light.night") && light == 2) {
					tb.setSelected(true);
				} else if (tb.getName().startsWith("mvt.")) {
					String c = tb.getName().replace("mvt.", "");
					if (mvt.contains(c)) {
						tb.setSelected(true);
					}
				}
			}
		}
		tfNotes.setText(scene.getNotes());
		if (scene.getMoment()==1) {
			in_ex.setText("INT");
		} else if (scene.getMoment()==2) {
			in_ex.setText("EXT");
		}
	}
	
	public void setScene(Scene scene) {
		Log.trace("Storyboard.setScene(scene="+scene.getName()+")");
		this.scene = scene;
		originalHash = getHashCode(scene);
		refresh();
		mainFrame.mainMenu.setBtScenes();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton bt = (JButton) e.getSource();
			switch (bt.getName()) {
				case "in_ex":
					if (bt.getText().equals("INT")) {
						bt.setText("EXT");
					} else {
						bt.setText("INT");
					}
					break;
				case "btImage":
					String imgFile = ImageChooserDlg.show(mainFrame);
					if (imgFile != null && !imgFile.isEmpty()) {
						refreshImage(imgFile);
					}
					break;
			}
		}
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
	}
	
	public void refreshScene() {
		pImage.removeAll();
		ImageIcon img = Images.getIcon("unknown");
		if (scene != null && !scene.getImg().isEmpty()) {
			img = Images.createImageIcon(scene.getImg());
			img = Images.resizeIcon(img, pImage.getWidth());
		}
		btImage = new JButton(img);
		btImage.setName("btImage");
		btImage.setMinimumSize(new Dimension(320, 240));
		btImage.addActionListener(this);
		pImage.add(btImage, "grow");
	}
	
	public void refreshImage(String imgFile) {
		if (btImage==null) refreshScene();
		ImageIcon img = Images.createExternalImageIcon(imgFile);
		if (img == null || imgFile.isEmpty()) {
			img=Images.getIcon("unknown");
			btImage.setIcon(img);
			return;
		}
		scene.setImg(imgFile);
		Dimension dim = pImage.getSize();
		if (dim.width==0) dim.width=640;
		if (dim.height==0) dim.height=480;
		dim.width = Math.min(640, dim.width);
		dim.height = Math.min(480, dim.height);
		img = Images.resizeIcon(img, Math.max(dim.width, dim.height));
		btImage.setIcon(img);
	}
	
	public boolean isModified() {
		Scene sc = getData();
		return (originalHash != getHashCode(sc));
	}
	
	private int getHashCode(Scene sc) {
		return sc.toXml().hashCode();
	}
	
	public Scene getData() {
		Scene sc = new Scene();
		sc.setSeq(Integer.parseInt(tfSeq.getText()));
		sc.setPlan(Integer.parseInt(tfPlan.getText()));
		sc.setDuration(Integer.parseInt(tfDur.getText()));
		int shot = 0;
		if (shot_normal.isSelected()) {
			shot = 1;
		} else if (shot_high.isSelected()) {
			shot = 2;
		} else if (shot_low.isSelected()) {
			shot = 3;
		}
		sc.setShot(shot);
		String focal = "0", mvt = "";
		Integer light = 0;
		for (Component comp : leftPanel.getComponents()) {
			if (comp instanceof JToggleButton) {
				JToggleButton tb = (JToggleButton) comp;
				if (tb.getName().startsWith("foc.") && tb.isSelected()) {
					focal = tb.getText();
				}
			}
		}
		for (Component comp : rightPanel.getComponents()) {
			if (comp instanceof JToggleButton) {
				JToggleButton tb = (JToggleButton) comp;
				if (tb.getName().equals("light.day") && tb.isSelected()) {
					light = 1;
				} else if (tb.getName().equals("light.night") && tb.isSelected()) {
					light = 2;
				} else if (tb.getName().startsWith("mvt.") && tb.isSelected()) {
					mvt += tb.getName().replace("mvt.", "");
				}
			}
		}
		sc.setFocal(Integer.parseInt(focal));
		sc.setMoment(light);
		sc.setMvt(mvt);
		if (in_ex.getText().equals("INT")) {
			sc.setLoc(1);
		} else if (in_ex.getText().equals("EXT")) {
			sc.setLoc(2);
		}
		sc.setNotes(tfNotes.getText());
		return sc;
	}
	
	public boolean check() {
		return(true);
	}
	
	public Scene apply() {
		return getData();
	}
	
}

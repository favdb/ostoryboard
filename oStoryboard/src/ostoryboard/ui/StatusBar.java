/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.ui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class StatusBar extends JPanel {

	private final MainFrame mainFrame;
	private JPanel right;
	private JPanel middle;
	private JPanel left;
	private MemoryPanel memPanel;

	public StatusBar(MainFrame mainFrame) {
		super();
		this.mainFrame=mainFrame;
		initAll();
	}
	
	private void initAll() {
		init();
		initUi();
	}
	
	private void init() {
		
	}
	
	private void initUi() {
		setLayout(new MigLayout("fill, ins 2","[]2[grow]2[]"));
		setBorder(BorderFactory.createEtchedBorder());
		left=new JPanel();
		left.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		left.add(new JLabel("    "));
		add(left,"al left");
		//add(new JSeparator());
		middle=new JPanel(new MigLayout("fill"));
		middle.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		middle.add(new JLabel("middle"), "growx");
		add(middle,"al center, growx");
		//add(new JSeparator());
		right=new JPanel();
		right.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		memPanel = new MemoryPanel();
		right.add(memPanel);
		add(right,"al right");
	}
	
	public void refresh() {
		memPanel.repaint();
	}
	
}

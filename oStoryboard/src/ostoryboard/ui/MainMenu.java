/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ostoryboard.ui;

import java.awt.Dimension;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import ostoryboard.i18n.I18Nmsg;
import ostoryboard.model.Scene;
import ostoryboard.resources.images.Images;
import ostoryboard.util.Log;

/**
 *
 * @author favdb
 */
public class MainMenu {

	private static final I18Nmsg i18n = I18Nmsg.getInstance();
	private final MainFrame mainFrame;
	private JMenuBar menubar;
	private JToolBar toolbar;
	public JComboBox cbScenes;
	private JButton btFirst;
	private JButton btPrevious;
	private JButton btNext;
	private JButton btLast;
	private JButton btNew;

	public MainMenu(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		initAll();
	}

	private void initAll() {
		init();
		initUi();
	}

	private void init() {

	}

	private void initUi() {
		menubar = new JMenuBar();
		initFileMenu();
		toolbar = new JToolBar();
		initFileTB();
		initScenesTB();
	}

	public JMenuBar getMenubar() {
		return menubar;
	}

	public JToolBar getToolbar() {
		return toolbar;
	}

	private void initFileMenu() {
		JMenu menuFile = new JMenu(i18n.str("file"));
		menuFile.setMnemonic(i18n.mnemo("file"));
		menuFile.add(initMenuItem("file.new"));
		menuFile.add(initMenuItem("file.open"));
		menuFile.add(initMenuItem("file.save"));
		menuFile.add(initMenuItem("file.saveas"));
		menuFile.add(new JSeparator());
		menuFile.add(initMenuItem("file.exit"));
		menubar.add(menuFile);
	}
	
	private JMenuItem initMenuItem(String name) {
		JMenuItem item = new JMenuItem(i18n.str(name));
		item.setName(name);
		item.setMnemonic(i18n.mnemo(name));
		item.addActionListener(mainFrame);
		return item;
	}

	private void initFileTB() {
		toolbar.add(initButton("file.new"));
		toolbar.add(initButton("file.open"));
		toolbar.add(initButton("file.save"));
		toolbar.add(new JSeparator());
	}
	
	private void initScenesTB() {
		toolbar.add(btFirst=initButton("scene.first", "nav-first"));
		toolbar.add(btPrevious=initButton("scene.previous", "nav-previous"));
		toolbar.add(cbScenes=initCombo("scene.list",mainFrame.getScenes()));
		toolbar.add(btNew=initButton("scene.new", "new"));
		toolbar.add(btNext=initButton("scene.next", "nav-next"));
		toolbar.add(btLast=initButton("scene.last", "nav-last"));
		toolbar.add(new JSeparator());
		refreshCbScenes();
	}
	
	@SuppressWarnings("unchecked")
	public void refeshScenesTB(String str) {
		Log.trace("MainMenu.refeshScenesTB(str="+str+")");
		refreshCbScenes();
		cbScenes.setSelectedItem(str);
	}

	@SuppressWarnings("unchecked")
	public void refeshScenesTB(int idx) {
		cbScenes.removeActionListener(mainFrame);
		cbScenes.removeAllItems();
		for (Scene scene:mainFrame.getScenes()) {
			cbScenes.addItem(scene.getName());
		}
		cbScenes.addActionListener(mainFrame);
		cbScenes.setSelectedIndex(idx);
	}

	private JButton initButton(String name) {
		JButton bt = new JButton();
		bt.setName(name);
		bt.setIcon(Images.getIcon(name.replace('.', '-')));
		bt.setToolTipText(i18n.str(name));
		bt.addActionListener(mainFrame);
		return bt;
	}
	
	private JButton initButton(String name, String icon) {
		JButton bt = new JButton();
		bt.setName(name);
		bt.setIcon(Images.getIcon(icon));
		bt.setToolTipText(i18n.str(name));
		bt.addActionListener(mainFrame);
		return bt;
	}
	
	private JComboBox initCombo(String name, List list) {
		JComboBox cb=new JComboBox();
		cb.setName(name);
		Dimension sz=cb.getMinimumSize();
		sz.width=sz.height*8;
		//cb.setPreferredSize(sz);
		cb.addActionListener(mainFrame);
		return cb;
	}
	
	@SuppressWarnings("unchecked")
	private void refreshCbScenes() {
		cbScenes.removeActionListener(mainFrame);
		int idx=cbScenes.getSelectedIndex();
		cbScenes.removeAllItems();
		for (Scene scene:mainFrame.getScenes()) {
			String str=String.format("%02d/%02d", scene.getSeq(), scene.getPlan());
			cbScenes.addItem(str);
		}
		if (idx>-1) {
			cbScenes.setSelectedIndex(idx);
		}
		else {
			cbScenes.setSelectedIndex(0);
		}
		cbScenes.addActionListener(mainFrame);
		setBtScenes();
	}
	
	public void setBtScenes() {
		int idx=cbScenes.getSelectedIndex();
		btFirst.setEnabled(idx!=0);
		btPrevious.setEnabled(idx!=0);
		btNext.setEnabled(idx+1<mainFrame.getScenes().size());
		btLast.setEnabled(idx+1<mainFrame.getScenes().size());
	}

}
